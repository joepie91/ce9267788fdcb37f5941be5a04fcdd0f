Some stuff I've had to figure out that wasn't documented...

## Proprietary AMD drivers (`fglrx`)

If you get this error:

```
/tmp/nix-build-ati-drivers-15.7-4.4.18.drv-0/common/lib/modules/fglrx/build_mod/2.6.x/firegl_public.c:194:22: fatal error: asm/i387.h: No such file or directory
```

... it's because the drivers are not compatible with your current kernel version. I've worked around it by adding this to my `configuration.nix`, to switch to a 4.1 kernel:

```nix
{
  # ...
  boot.kernelPackages = plgs.linuxPackages_4_1;
  # ...
}
```

## Installing a few packages from `master`

1. `git clone https://github.com/NixOS/nixpkgs.git /etc/nixos/nixpkgs-master`
2. Edit your `/etc/nixos/configuration.nix` like this:

```nix
{ config, pkgs, ... }:

let
  nixpkgsMaster = import ./nixpkgs-master {};
  
  stablePackages = with pkgs; [
    # This is where your packages from stable nixpkgs go
  ];
  
  masterPackages = with nixpkgsMaster; [
    # This is where your packages from `master` go
    nodejs-6_x
  ];
in {
  # This is where your normal config goes, we've just added a `let` block
  
  environment = {
    # ...
    
    systemPackages = stablePackages ++ masterPackages;
  };
  
  # ...
}
```

## GRUB2 on UEFI

This works fine. You need your `boot` section configured like this:

```nix
{
  # ...
  boot = {
    loader = {
      gummiboot.enable = false;
      
      efi = {
        canTouchEfiVariables = true;
      };
      
      grub = {
        enable = true;
        device = "nodev";
        version = 2;
        efiSupport = true;
      };
    };
  };
  # ...
}
```

## Unblock ports in the firewall

The firewall is enabled by default. This is how you open a port:

```nix
{
  # ...
  networking = {
    # ...
    
    firewall = {
      allowedTCPPorts = [ 24800 ];
    };
  };
  # ...
}
```

## Guake doesn't start because of a GConf issue

1. Read the following warning: __GNOME's GConf implements a system-wide registry (like on Windows) that applications can use to store and retrieve internal configuration data. That concept is inherently impure, and it's very hard to support on NixOS.__
2. ... but, Guake isn't going to work otherwise, so follow the instructions [here](https://web.archive.org/web/20160829175357/https://nixos.org/wiki/Solve_GConf_errors_when_running_GNOME_applications).
3. Run the following to set up the GConf schema for Guake: `gconftool-2 --install-schema-file $(readlink $(which guake) | grep -Eo '\/nix\/store\/[^\/]+\/')"share/gconf/schemas/guake.schemas"`. This will not work if you have changed your Nix store path - in that case, modify the command accordingly.

You may need to re-login to make the changes apply.

## FFMpeg support in `youtube-dl`

Based on [this post](https://github.com/NixOS/nixpkgs/issues/5236#issuecomment-139865161):

```nix
{
  # ...
  stablePackages = with pkgs; [
    # ...
    (python35Packages.youtube-dl.override {
      ffmpeg = ffmpeg-full;
    })
    # ...
  ];
  # ...
}
```

(To understand what `stablePackages` is here, see [this entry](https://gist.github.com/joepie91/ce9267788fdcb37f5941be5a04fcdd0f#installing-a-few-packages-from-master).)